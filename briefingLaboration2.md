<center> <h1>Laboration 2: Do the Maths</h1> </center>
<center><h2>DT044A Data Mining<h2></center>
<center><h2>Mid Sweden University<h2></center>

In your second laboration, you will collect practical experience on the creation of basic classifiers.
There is no right or wrong order in which to do the exercises. None of the results are supposed to be submitted.

**Important Notice: Evaluation is the topic of the next lecture. Until then, we simply pretend as if it was fine to use the same data instances that we use for the building of the classifiers also when testing/evaluating the accuracy of the classifier. We will learn in lecture 3 why this has to be avoided at all costs, and how this relates to overfitting.**

## Calculate
In lecture 2, we went through the basic machine learning algorithms for data mining. Among them, ZeroR, OneR, Naive Bayes, and Decision Trees. Running the algorithms in tools, such as [Rapid Miner](https://rapidminer.com/), or [Weka](http://www.cs.waikato.ac.nz/ml/weka/), is not hard. But in order to get a better feeling for how the methods work internally, this exercise is about creating the classifiers **by hand** on toy problems.

### 1. Balloon data set

The balloon data set can be downloaded [here](https://dl.dropboxusercontent.com/u/35363454/datamining/data/balloon.csv) and is shown in the following table:

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;border-top-width:1px;border-bottom-width:1px;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;border-top-width:1px;border-bottom-width:1px;}
.tg .tg-e3zv{font-weight:bold}
.tg .tg-4eph{background-color:#f9f9f9}
.tg .tg-b7b8{background-color:#f9f9f9;vertical-align:top}
.tg .tg-yw4l{vertical-align:top}
</style>
<table class="tg">
  <tr>
    <th class="tg-e3zv">id</th>
    <th class="tg-e3zv">color</th>
    <th class="tg-e3zv">size</th>
    <th class="tg-e3zv">act</th>
    <th class="tg-e3zv">age</th>
    <th class="tg-e3zv">inflated</th>
  </tr>
  <tr>
    <td class="tg-4eph">1</td>
    <td class="tg-4eph">YELLOW</td>
    <td class="tg-4eph">SMALL</td>
    <td class="tg-4eph">STRETCH</td>
    <td class="tg-4eph">ADULT</td>
    <td class="tg-4eph">TRUE</td>
  </tr>
  <tr>
    <td class="tg-031e">2</td>
    <td class="tg-031e">YELLOW</td>
    <td class="tg-031e">SMALL</td>
    <td class="tg-031e">STRETCH</td>
    <td class="tg-031e">CHILD</td>
    <td class="tg-031e">TRUE</td>
  </tr>
  <tr>
    <td class="tg-4eph">3</td>
    <td class="tg-4eph">YELLOW</td>
    <td class="tg-4eph">SMALL</td>
    <td class="tg-4eph">DIP</td>
    <td class="tg-4eph">ADULT</td>
    <td class="tg-4eph">TRUE</td>
  </tr>
  <tr>
    <td class="tg-031e">4</td>
    <td class="tg-031e">YELLOW</td>
    <td class="tg-031e">SMALL</td>
    <td class="tg-031e">DIP</td>
    <td class="tg-031e">CHILD</td>
    <td class="tg-031e">TRUE</td>
  </tr>
  <tr>
    <td class="tg-4eph">5</td>
    <td class="tg-4eph">YELLOW</td>
    <td class="tg-4eph">LARGE</td>
    <td class="tg-4eph">STRETCH</td>
    <td class="tg-4eph">ADULT</td>
    <td class="tg-4eph">TRUE</td>
  </tr>
  <tr>
    <td class="tg-031e">6</td>
    <td class="tg-031e">YELLOW</td>
    <td class="tg-031e">LARGE</td>
    <td class="tg-031e">STRETCH</td>
    <td class="tg-031e">CHILD</td>
    <td class="tg-031e">FALSE</td>
  </tr>
  <tr>
    <td class="tg-4eph">7</td>
    <td class="tg-4eph">YELLOW</td>
    <td class="tg-4eph">LARGE</td>
    <td class="tg-4eph">DIP</td>
    <td class="tg-4eph">ADULT</td>
    <td class="tg-4eph">FALSE</td>
  </tr>
  <tr>
    <td class="tg-031e">8</td>
    <td class="tg-031e">YELLOW</td>
    <td class="tg-031e">LARGE</td>
    <td class="tg-031e">DIP</td>
    <td class="tg-031e">CHILD</td>
    <td class="tg-031e">FALSE</td>
  </tr>
  <tr>
    <td class="tg-4eph">9</td>
    <td class="tg-4eph">PURPLE</td>
    <td class="tg-4eph">SMALL</td>
    <td class="tg-4eph">STRETCH</td>
    <td class="tg-4eph">ADULT</td>
    <td class="tg-4eph">TRUE</td>
  </tr>
  <tr>
    <td class="tg-031e">10</td>
    <td class="tg-031e">PURPLE</td>
    <td class="tg-031e">SMALL</td>
    <td class="tg-031e">STRETCH</td>
    <td class="tg-031e">CHILD</td>
    <td class="tg-031e">FALSE</td>
  </tr>
  <tr>
    <td class="tg-4eph">11</td>
    <td class="tg-4eph">PURPLE</td>
    <td class="tg-4eph">SMALL</td>
    <td class="tg-4eph">DIP</td>
    <td class="tg-4eph">ADULT</td>
    <td class="tg-4eph">FALSE</td>
  </tr>
  <tr>
    <td class="tg-031e">12</td>
    <td class="tg-031e">PURPLE</td>
    <td class="tg-031e">SMALL</td>
    <td class="tg-031e">DIP</td>
    <td class="tg-031e">CHILD</td>
    <td class="tg-031e">FALSE</td>
  </tr>
  <tr>
    <td class="tg-4eph">13</td>
    <td class="tg-4eph">PURPLE</td>
    <td class="tg-4eph">LARGE</td>
    <td class="tg-4eph">STRETCH</td>
    <td class="tg-4eph">ADULT</td>
    <td class="tg-4eph">TRUE</td>
  </tr>
  <tr>
    <td class="tg-031e">14</td>
    <td class="tg-031e">PURPLE</td>
    <td class="tg-031e">LARGE</td>
    <td class="tg-031e">STRETCH</td>
    <td class="tg-031e">CHILD</td>
    <td class="tg-031e">FALSE</td>
  </tr>
  <tr>
    <td class="tg-b7b8">15</td>
    <td class="tg-b7b8">PURPLE</td>
    <td class="tg-b7b8">LARGE</td>
    <td class="tg-b7b8">DIP</td>
    <td class="tg-b7b8">ADULT</td>
    <td class="tg-b7b8">FALSE</td>
  </tr>
  <tr>
    <td class="tg-yw4l">16</td>
    <td class="tg-yw4l">PURPLE</td>
    <td class="tg-yw4l">LARGE</td>
    <td class="tg-yw4l">DIP</td>
    <td class="tg-yw4l">CHILD</td>
    <td class="tg-yw4l">FALSE</td>
  </tr>
</table>

1. How can we create a ZeroR classifier for the target variable **inflated**?
	* What is the accuracy if used on the training set?
2. How about OneR? Which attribute would OneR choose and why?
3. Create a Decision Tree using the gain metric in order to minimize tree size. Use Pre-pruning, to ensure that no leafs have less than 2 entries.
4. Download [this](https://dl.dropboxusercontent.com/u/35363454/datamining/data/balloon_missing.csv) version of the data set with missing values. How does the missing data impact ZeroR and OneR?

You can verify your calculations by importing the balloon data set into Weka and running the different classifiers. Follow the installation instructions in section 3., further below.


### 2. O-ring data set

The O-ring data set can be downloaded [here](https://dl.dropboxusercontent.com/u/35363454/datamining/data/o-ring.csv) and is shown in the following table:

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;border-top-width:1px;border-bottom-width:1px;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;border-top-width:1px;border-bottom-width:1px;}
.tg .tg-e3zv{font-weight:bold}
.tg .tg-4eph{background-color:#f9f9f9}
.tg .tg-b7b8{background-color:#f9f9f9;vertical-align:top}
.tg .tg-yw4l{vertical-align:top}
</style>
<table class="tg">
  <tr>
    <th class="tg-e3zv">numberRings</th>
    <th class="tg-e3zv">numberOringsWithStress</th>
    <th class="tg-e3zv">launchTemperature</th>
    <th class="tg-e3zv">leackCheckPressure</th>
    <th class="tg-e3zv">id</th>
  </tr>
  <tr>
    <td class="tg-4eph">6</td>
    <td class="tg-4eph">0</td>
    <td class="tg-4eph">66</td>
    <td class="tg-4eph">50</td>
    <td class="tg-4eph">1</td>
  </tr>
  <tr>
    <td class="tg-031e">6</td>
    <td class="tg-031e">1</td>
    <td class="tg-031e">70</td>
    <td class="tg-031e">50</td>
    <td class="tg-031e">2</td>
  </tr>
  <tr>
    <td class="tg-4eph">6</td>
    <td class="tg-4eph">0</td>
    <td class="tg-4eph">69</td>
    <td class="tg-4eph">50</td>
    <td class="tg-4eph">3</td>
  </tr>
  <tr>
    <td class="tg-031e">6</td>
    <td class="tg-031e">0</td>
    <td class="tg-031e">68</td>
    <td class="tg-031e">50</td>
    <td class="tg-031e">4</td>
  </tr>
  <tr>
    <td class="tg-4eph">6</td>
    <td class="tg-4eph">0</td>
    <td class="tg-4eph">67</td>
    <td class="tg-4eph">50</td>
    <td class="tg-4eph">5</td>
  </tr>
  <tr>
    <td class="tg-031e">6</td>
    <td class="tg-031e">0</td>
    <td class="tg-031e">72</td>
    <td class="tg-031e">50</td>
    <td class="tg-031e">6</td>
  </tr>
  <tr>
    <td class="tg-4eph">6</td>
    <td class="tg-4eph">0</td>
    <td class="tg-4eph">73</td>
    <td class="tg-4eph">100</td>
    <td class="tg-4eph">7</td>
  </tr>
  <tr>
    <td class="tg-031e">6</td>
    <td class="tg-031e">0</td>
    <td class="tg-031e">70</td>
    <td class="tg-031e">100</td>
    <td class="tg-031e">8</td>
  </tr>
  <tr>
    <td class="tg-4eph">6</td>
    <td class="tg-4eph">1</td>
    <td class="tg-4eph">57</td>
    <td class="tg-4eph">200</td>
    <td class="tg-4eph">9</td>
  </tr>
  <tr>
    <td class="tg-031e">6</td>
    <td class="tg-031e">1</td>
    <td class="tg-031e">63</td>
    <td class="tg-031e">200</td>
    <td class="tg-031e">10</td>
  </tr>
  <tr>
    <td class="tg-4eph">6</td>
    <td class="tg-4eph">1</td>
    <td class="tg-4eph">70</td>
    <td class="tg-4eph">200</td>
    <td class="tg-4eph">11</td>
  </tr>
  <tr>
    <td class="tg-031e">6</td>
    <td class="tg-031e">0</td>
    <td class="tg-031e">78</td>
    <td class="tg-031e">200</td>
    <td class="tg-031e">12</td>
  </tr>
  <tr>
    <td class="tg-4eph">6</td>
    <td class="tg-4eph">0</td>
    <td class="tg-4eph">67</td>
    <td class="tg-4eph">200</td>
    <td class="tg-4eph">13</td>
  </tr>
  <tr>
    <td class="tg-031e">6</td>
    <td class="tg-031e">2</td>
    <td class="tg-031e">53</td>
    <td class="tg-031e">200</td>
    <td class="tg-031e">14</td>
  </tr>
  <tr>
    <td class="tg-4eph">6</td>
    <td class="tg-4eph">0</td>
    <td class="tg-4eph">67</td>
    <td class="tg-4eph">200</td>
    <td class="tg-4eph">15</td>
  </tr>
  <tr>
    <td class="tg-031e">6</td>
    <td class="tg-031e">0</td>
    <td class="tg-031e">75</td>
    <td class="tg-031e">200</td>
    <td class="tg-031e">16</td>
  </tr>
  <tr>
    <td class="tg-b7b8">6</td>
    <td class="tg-b7b8">0</td>
    <td class="tg-b7b8">70</td>
    <td class="tg-b7b8">200</td>
    <td class="tg-b7b8">17</td>
  </tr>
  <tr>
    <td class="tg-yw4l">6</td>
    <td class="tg-yw4l">0</td>
    <td class="tg-yw4l">81</td>
    <td class="tg-yw4l">200</td>
    <td class="tg-yw4l">18</td>
  </tr>
  <tr>
    <td class="tg-b7b8">6</td>
    <td class="tg-b7b8">0</td>
    <td class="tg-b7b8">76</td>
    <td class="tg-b7b8">200</td>
    <td class="tg-b7b8">19</td>
  </tr>
  <tr>
    <td class="tg-yw4l">6</td>
    <td class="tg-yw4l">0</td>
    <td class="tg-yw4l">79</td>
    <td class="tg-yw4l">200</td>
    <td class="tg-yw4l">20</td>
  </tr>
  <tr>
    <td class="tg-b7b8">6</td>
    <td class="tg-b7b8">0</td>
    <td class="tg-b7b8">75</td>
    <td class="tg-b7b8">200</td>
    <td class="tg-b7b8">21</td>
  </tr>
  <tr>
    <td class="tg-yw4l">6</td>
    <td class="tg-yw4l">0</td>
    <td class="tg-yw4l">76</td>
    <td class="tg-yw4l">200</td>
    <td class="tg-yw4l">22</td>
  </tr>
  <tr>
    <td class="tg-b7b8">6</td>
    <td class="tg-b7b8">1</td>
    <td class="tg-b7b8">58</td>
    <td class="tg-b7b8">200</td>
    <td class="tg-b7b8">23</td>
  </tr>
</table>

Find additional information about the data set [here](https://archive.ics.uci.edu/ml/datasets/Challenger+USA+Space+Shuttle+O-Ring).

1. From those classifiers we've got to know in lecture 2, which ones can we straight away use to predict the number of *O-rings with stress* for the next flight?
	* Why is OneR not among them? How to change that?
3. Is there an attribute in the data set that is introducing noise?
2. Build a Naive-Bayes classifier based on the entire data set. What is the accuracy when the classifier is tested over the entire data set?

Verify your results using Weka.

### 3. Weka Questions (Warm Up)

In order to get started with Weka, Download it from [here](http://prdownloads.sourceforge.net/weka/weka-3-6-13.zip), unzip it in a folder on your harddisk, and double click the file *weka.jar* to start the application. The tool we use in this course is the *Explorer*.

1. In the Explorer, open the contact-lenses dataset by pressing "Open file..." and choosing the file in "data/contact-lenses.arff". How many instances are there?
	* 5, 8, 24, or 32

2. How many attributes are there?
	* 4, 5, 12, 24

3. How many possible values are there for the age attribute?
	*2, 3, 5 or 8

4. Which of these attributes has *reduced* as a possible value?
	* age, spectacle-prescrip, astigmatism, tear-prod-rate, contact-lenses

5. In the Explorer, open the contact-lenses dataset by pressing "Open file..." and choosing the file in "data/iris.arff". How many instances are there?
	* 100, 150, 200, or 250

6. How many attributes are there?
	* 4, 5, 7 or 10

7. How many possible values does the class attribute have?
	* 1, 2, 3 or 50

8. Do an image search on the web to find pictures of Iris setosa, Iris virginica and Iris versicolor to see what the different types look like.

9. Label these images of irises according to their type by choosing the correct sequence:
<center>
<img src="https://dl.dropboxusercontent.com/u/35363454/datamining/flowers.png" alt="flowers" style="width:400px"/>
</center>
   * (a) setosa (b) virginica (c) versicolor
   * (a) setosa (b) versicolor (c) virginica
   * (a) versicolor (b) virginica (c) setosa
   * (a) virginica (b) versicolor (c) setosa

10. Does the class Iris-setosa tend to have high or low values of sepallength?
	* low or high

11. Does the class Iris-virginica tend to have high or low values of petalwidth?
	* low or high

12. Which of these attributes, taken by itself, gives the best indication of the class?
	* sepallength, sepalwidth or petalwidth

13. Examine the Iris ARFF file header and say when the dataset was first used?
	* 1936, 1973, 1980 or 1988

10. Weka can read Comma Separated Values (.csv) format files by selecting the appropriate File Format in the "Open" file dialog. Ascertain by experiment how Weka determines the attribute names and value sets by creating a small spreadsheet file, saving it in Comma Separated Values (.csv) format, and loading it into Weka.

11. What should be the first row of a Comma Separated Values (.csv) format file that contains the nominal Weather data?
 * outlook,temperature,humidity,windy,play
 * sunny,hot,high,FALSE,no
 * sunny,hot,high,TRUE,no
 * rainy,mild,high,TRUE,no

<p align="right">*Felix Dobslaw* - felix.dobslaw@miun.se</p>
